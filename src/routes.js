angular
  .module('app')
  .config(routesConfig);

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('listaCompromissos', {
      url: '/',
      component: 'listaCompromissos'
    })
    .state('compromissos', {
      url: '/adiciona',
      component: 'compromissos'
    });
}
