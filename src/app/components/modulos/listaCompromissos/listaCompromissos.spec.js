describe('listaCompromissos component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('listaCompromissos', function () {
      return {
        templateUrl: 'app/listaCompromissos.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<listaCompromissos></listaCompromissos>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
