function listaCompromissosController() {
  this.text = 'My brand new component!';
}

angular
  .module('app')
  .component('listaCompromissos', {
    templateUrl: 'app/components/modulos/listaCompromissos/listaCompromissos.html',
    controller: listaCompromissosController
  });

