describe('compromissos component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('compromissos', function () {
      return {
        templateUrl: 'app/compromissos.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<compromissos></compromissos>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
