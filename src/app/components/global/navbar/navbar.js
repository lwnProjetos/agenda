function navbarController() {
  this.text = 'navbar';
}

angular
  .module('app')
  .component('navbar', {
    templateUrl: 'app/components/global/navbar/navbar.html',
    controller: navbarController
  });

