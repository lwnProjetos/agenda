function footerController() {
  this.text = 'My brand new component!';
}

angular
  .module('app')
  .component('footer', {
    templateUrl: 'app/components/global/footer/footer.html',
    controller: footerController
  });

