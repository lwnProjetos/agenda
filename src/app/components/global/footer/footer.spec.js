describe('footer component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('footer', function () {
      return {
        templateUrl: 'app/footer.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<footer></footer>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
